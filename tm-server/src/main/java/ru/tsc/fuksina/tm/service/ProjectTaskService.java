package ru.tsc.fuksina.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.repository.IProjectRepository;
import ru.tsc.fuksina.tm.api.repository.ITaskRepository;
import ru.tsc.fuksina.tm.api.service.IConnectionService;
import ru.tsc.fuksina.tm.api.service.IProjectTaskService;
import ru.tsc.fuksina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.fuksina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.fuksina.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.fuksina.tm.exception.field.TaskIdEmptyException;
import ru.tsc.fuksina.tm.exception.field.UserIdEmptyException;
import ru.tsc.fuksina.tm.dto.model.TaskDTO;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull SqlSession getSqlSession() {
        return connectionService.getSqlSession();
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).filter(item -> !item.isEmpty()).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).filter(item -> !item.isEmpty()).orElseThrow(TaskIdEmptyException::new);
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final TaskDTO task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                    .orElseThrow(TaskNotFoundException::new);
            task.setProjectId(projectId);
            taskRepository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).filter(item -> !item.isEmpty()).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).filter(item -> !item.isEmpty()).orElseThrow(TaskIdEmptyException::new);
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final TaskDTO task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                    .orElseThrow(TaskNotFoundException::new);
            task.setProjectId(null);
            taskRepository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).filter(item -> !item.isEmpty()).orElseThrow(ProjectIdEmptyException::new);
        try (@NotNull final SqlSession sqlSession = getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            try {
                @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(userId, projectId);
                tasks.stream()
                        .forEach(task -> taskRepository.removeById(userId, task.getId()));
                projectRepository.removeById(userId, projectId);
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
    }

}
