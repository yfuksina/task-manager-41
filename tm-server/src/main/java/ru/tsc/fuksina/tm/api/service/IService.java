package ru.tsc.fuksina.tm.api.service;

import ru.tsc.fuksina.tm.api.repository.IRepository;
import ru.tsc.fuksina.tm.dto.model.AbstractModelDTO;

public interface IService<M extends AbstractModelDTO> extends IRepository<M> {

}
