package ru.tsc.fuksina.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseSqlDialect();

    @NotNull
    String getDatabaseHbm2ddlAuto();

    @NotNull
    String getDatabaseShowSql();

}
